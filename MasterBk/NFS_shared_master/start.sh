#!/bin/sh

sed -i'' -e '/condor-master/d' /root/.ssh/authorized_keys
cat /local/condor/authorized_keys >> /root/.ssh/authorized_keys

CONDOR_IP=`ip addr show eth1 | grep -Po 'inet \K[\d.]+'`

mount /dev/disk/by-label/CONTEXT /mnt
. /mnt/context.sh
umount /mnt

echo "WORKER_DAEMON_LIST=$WORKER_DAEMON_LIST" > /etc/condor/config.d/daemons.conf
echo "CONDOR_NETWORK_INTERFACE=$CONDOR_IP" > /etc/condor/config.d/interface.conf

. `dirname $0`/$WORKER_DAEMON_LIST.sh

sed -i'' -e '/condor-self/d' /etc/hosts
echo "$CONDOR_IP $CONDOR_NAME condor-self" >> /etc/hosts

service condor start
