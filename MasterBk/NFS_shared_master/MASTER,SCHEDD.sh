CONDOR_NAME=`hostname`

VG=disk0-vg
DISK=/dev/$VG/disk0
MP=/disk0

if [ ! -d "$MP" ]; then
	pvcreate /dev/vdb
	vgcreate $VG /dev/vdb
	lvcreate $VG -l 100%VG -n disk0
	mkfs.ext4 $DISK
	mkdir $MP

	if [ -d /home ]; then
		mv /home "/home.`date`"
	fi
fi

mount $DISK $MP

ln -sf /net/$CONDOR_IP$MP /home

for d in /dev/vd[c-z]; do
	pvcreate $d
	vgextend $VG $d
done
lvextend -l 100%VG $DISK
resize2fs $DISK

ESCMP=`echo $MP | sed -e 's!/!\\\\/!g'`
sed -i'' -e "/$ESCMP/d" /etc/exports
echo "$MP	172.16.0.0/16(rw,async,no_subtree_check)" >> /etc/exports
service nfs-kernel-server start
